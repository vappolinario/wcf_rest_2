#include "rapidjson/document.h"
#include <sstream>
#include <iostream>

#include "rest_client.h"

using namespace rapidjson;

int main (int /*argc*/, char** /*argv*/)
{
	// Client da libcurl
	RestClient client;
	auto data = client.DownloadData("http://localhost:8090/library/books/sections/");
	// parser da rapidjson
	Document library;
	library.Parse(data.c_str());
	// obtendo os valores de uma propriedade
	const Value& sections = library["Sections"];
	// percorrendo os valores de um array json
	for (auto libItems = sections.Begin(); libItems != sections.End(); ++libItems)
	{
		std::cout << "Na Seção '" << libItems->GetString() << "' temos: " << std::endl;
		// outra requisição para obter valores de outro endpoint
		data = client.DownloadData("http://localhost:8090/library/books/items/" + std::string(libItems->GetString()));
		// novo parser para outro objeto json recebido
		Document section;
		section.Parse(data.c_str());
		// acessando os valores do array de livros
		const Value& books = section["Books"];
		for (rapidjson::SizeType currentBook = 0; currentBook < books.Size(); currentBook++)
		{
			const Value& book = books[currentBook];
			std::cout << book["Copies"].GetInt() << " cópias do '" << book["Title"].GetString() << "' de " << book["Author"].GetString() << std::endl;
		}
	}
}

