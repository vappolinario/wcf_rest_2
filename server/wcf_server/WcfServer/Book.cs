﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace WcfServer
{
	[DataContract]
	public class Book
	{
		public Book(string title, string author, int copies)
		{
			Title = title;
			Author = author;
			Copies = copies;
		}

		[DataMember]
		public string Title;
		[DataMember]
		public string Author;
		[DataMember]
		public int Copies;
	}
}
