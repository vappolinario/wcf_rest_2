﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace WcfServer
{
	public class LibraryService : IBooks
	{
		[WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "sections/")]
		public Library GetSections()
		{
			var lib = new Library();
			lib.Sections = new List<string>() { "ScienceFiction", "Fantasy" };
			return lib;
		}

		[WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "items/{section}")]
		public Section GetBooks(string section)
		{
			if (section.Equals("ScienceFiction", StringComparison.CurrentCultureIgnoreCase))
			{
				return new Section("ScienceFiction", new List<Book>() 
					{ 
						new Book("I Robot", "Isaac Asimov", 10), 
						new Book("Old Man's War", "John Scalzi", 5), 
						new Book("Ender's Game", "Orson Scott Card", 5) 
					});
			}
			else if (section.Equals("Fantasy", StringComparison.CurrentCultureIgnoreCase))
			{
				return new Section("Fantasy", new List<Book>() 
					{ 
						new Book ("The Hobbit", "J.R.R. Tolkien", 5),
						new Book ("A Game of Thrones", "George R. R. Martin", 3)
					});
			}
			return new Section();
		}
	}
}
