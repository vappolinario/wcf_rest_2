﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WcfServer
{
	[DataContract]
	public class Section
	{
		public Section()
		{
			Name = "";
			Books = new List<Book>();
		}

		public Section(string name, List<Book> books)
		{
			Name = name;
			Books = books;
		}
		[DataMember]
		public string Name;
		[DataMember]
		public List<Book> Books;
	}
}
