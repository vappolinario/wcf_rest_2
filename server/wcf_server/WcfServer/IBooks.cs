﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace WcfServer
{
	[ServiceContract]
	public interface IBooks
	{
		[OperationContract]
		Library GetSections();
		[OperationContract]
		Section GetBooks(string section);
	}
}
